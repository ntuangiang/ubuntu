#!/bin/sh

set -e

# Install kubectl binary with curl on Linux
#curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
#curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.18.0/bin/linux/amd64/kubectl
#chmod +x $HOME/./kubectl
#sudo mv $HOME/./kubectl /usr/local/bin/kubectl
#kubectl version --client

# Install using native package management
#sudo apt-get update && sudo apt-get install -y apt-transport-https gnupg2
#curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -

#echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
#sudo apt-get update
#sudo apt-get install -y kubectl

# Enabling shell autocompletion
#echo 'source <(kubectl completion zsh)' >> $HOME/.zshrc
#echo 'alias k=kubectl' >> $HOME/.zshrc
#echo 'complete -F __start_kubectl k' >> $HOME/.zshrc

#if [ -x "$(command -v compinit)" ] && [ -x "$(command -v autoload)" ]; then
#   autoload -Uz compinit
#   compinit
#fi

# Letting iptables see bridged traffic
##cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
##net.bridge.bridge-nf-call-ip6tables = 1
#net.bridge.bridge-nf-call-iptables = 1
#EOF
#sudo sysctl --system

# Installing kubeadm, kubelet and kubectl
#sudo apt-get update && sudo apt-get install -y apt-transport-https curl
#sudo apt-get update
#sudo apt-get install -y kubelet kubeadm kubectl
#sudo apt-mark hold kubelet kubeadm kubectl

# Create a single-host Kubernetes cluster
#sudo swapoff -a
#sudo kubeadm init --pod-network-cidr=10.244.0.0/16
#mkdir -p $HOME/.kube
#sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
#sudo chown $(id -u):$(id -g) $HOME/.kube/config
#sudo kubectl create -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
#kubectl taint nodes --all node-role.kubernetes.io/master-
#kubectl get nodes -o wide

# Install Helm
#curl https://helm.baltorepo.com/organization/signing.asc | sudo apt-key add -
#sudo apt-get install apt-transport-https --yes
#echo "deb https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
#sudo apt-get update
#sudo apt-get install -y helm

