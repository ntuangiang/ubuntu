#!/bin/sh

set -e

if [ -d "${HOME}/.oh-my-zsh" ]; then
  rm -rf ${HOME}/.oh-my-zsh
fi

sudo apt install zsh fonts-powerline -y

git clone https://github.com/ohmyzsh/ohmyzsh.git ${HOME}/.oh-my-zsh
git clone https://github.com/bhilburn/powerlevel9k.git ${HOME}/.oh-my-zsh/custom/themes/powerlevel9k
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

cp ${HOME}/.oh-my-zsh/templates/zshrc.zsh-template ${HOME}/.zshrc
cp ${HOME}/.zshrc ${HOME}/.zshrc.orig

chsh -s $(which zsh)

sed -i 's/ZSH_THEME="robbyrussell"/ZSH_THEME="powerlevel9k\/powerlevel9k"/g' ${HOME}/.zshrc
sed -i 's/plugins=(git)/plugins=(git zsh-autosuggestions zsh-syntax-highlighting)/g' ${HOME}/.zshrc

echo 'export DEFAULT_USER="${USER}"' >> ${HOME}/.zshrc
echo 'export EDITOR=vim' >> ${HOME}/.zshrc
