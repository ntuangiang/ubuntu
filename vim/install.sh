#!/bin/sh

set -e

VIM=$HOME/.ubuntu/vim

sudo apt update
sudo apt install -y software-properties-common

#Install Python 3.8
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt update
sudo apt install python3.8 -y

# Install NodeJs 
curl -fsSL https://deb.nodesource.com/setup_15.x | sudo bash -
sudo apt-get install -y nodejs

curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update && sudo apt-get install yarn

sudo apt install build-essential cmake python3.8-dev -y

# Build Vim
vim +PlugInstall +qall

sudo apt install neovim exuberant-ctags ripgrep silversearcher-ag -y

cp $VIM/init.vim $HOME/.config/nvim/init.vim
cp $VIM/local.init.vim $HOME/.config/nvim/local.init.vim
cp $VIM/local.bundles.vim $HOME/.config/nvin/local.bundles.vim

sudo update-alternatives --install /usr/bin/vi vi /usr/bin/nvim 60
sudo update-alternatives --config vi
sudo update-alternatives --install /usr/bin/vim vim /usr/bin/nvim 60
sudo update-alternatives --config vim
sudo update-alternatives --install /usr/bin/editor editor /usr/bin/nvim 60
sudo update-alternatives --config editor
