" Theme
Plug 'projekt0n/github-nvim-theme'

" javascript
"" Javascript Bundle
Plug 'jelera/vim-javascript-syntax'


" typescript
Plug 'leafgarland/typescript-vim'
Plug 'HerringtonDarkholme/yats.vim'

" javascript autocomplete
Plug 'neoclide/coc.nvim', {'branch': 'release'}

