"" Script
let g:github_comment_style = "NONE"
let g:github_keyword_style = "NONE"
let g:github_function_style = "NONE"
let g:github_variable_style = "NONE"

" NERDTRee
let NERDTreeShowHidden=1

" Enable blinking together with different cursor shapes for insert/command mode, and cursor highlighting:
au VimEnter,VimResume * set guicursor=n-v-c:block,i-ci-ve:ver25,r-cr:hor20,o:hor50

" Load the colorscheme
colorscheme github_dark

if filereadable(expand("~/.config/nvim/local_coc.vim"))
  source ~/.config/nvim/local_coc.vim
endif

