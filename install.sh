#!/bin/sh

set -e

cd ~
CWD=$(pwd)/.ubuntu

if [ -d "${CWD}" ]; then
  rm -rf $CWD
fi

sudo add-apt-repository ppa:bamboo-engine/ibus-bamboo
sudo apt-get update
    
sudo apt-get install ibus-bamboo git vim curl  -y

ibus restart

# Clone Installer
git clone https://gitlab.com/ntuangiang/ubuntu.git $CWD

source $CWD/terminal/install.sh
source $CWD/vim/install.sh
source $CWD/docker/install.sh
source $CWD/kubernetes/install.sh

# Elasticsearch Required
# sudo echo "vm.max_map_count=262144" >> /etc/sysctl.conf

echo "Setup Successfully."
